3rd ICFA Beam Dynamics Mini-Workshop 2022
=========================================

This example code base uses the following packages:

- [cernml-coi](https://gitlab.cern.ch/be-op-ml-optimization/cernml-coi/)
- [cernml-coi-utils](https://gitlab.cern.ch/be-op-ml-optimization/cernml-coi-utils/)
- [cernml-coi-loops](https://gitlab.cern.ch/be-op-ml-optimization/cernml-coi-loops/)
  (*extremely* early prototype!)

All further dependencies (including PyTorch) are downloaded automatically
during installation.

Quickstart
----------

```shell-session
$ git clone https://gitlab.cern.ch/nmadysa/icfa-tutorial
$ cd icfa-tutorial
$ python -m venv .venv
$ source .venv/bin/activate
$ pip install git+https://gitlab.cern.ch/be-op-ml-optimization/cernml-coi
$ pip install git+https://gitlab.cern.ch/be-op-ml-optimization/cernml-coi-utils
$ pip install git+https://gitlab.cern.ch/be-op-ml-optimization/cernml-coi-loops
$ pip install --editable .
$ python miniopt.py
```
