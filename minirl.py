"""Show-case reinforcement learning."""

from contextlib import closing

from cernml import coi
from cernml.rltools.envloop import loop
from cernml.rltools.wrappers import DontRender, LogRewards, RenderOnStep
from matplotlib import pyplot
from stable_baselines3 import TD3

import icfa_tutorial.env


def main() -> None:
    """Main function."""
    env = LogRewards(DontRender(coi.make("FakeSteeringChronoEnv-v0")))
    agent = TD3("MlpPolicy", env, learning_rate=2e-3)
    with closing(env):
        agent.learn(total_timesteps=300)
        env.end_training()
        loop(agent, env, max_episodes=10)
    env.render("human")
    pyplot.tight_layout()
    pyplot.show()


if __name__ == "__main__":
    main()
