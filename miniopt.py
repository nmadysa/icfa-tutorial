"""Optimization runner for this tutorial."""

from __future__ import annotations

import typing as t
from array import array
from contextlib import closing

from cernml import loops
from cernml.loops.adapters.scipy import Cobyla
from matplotlib import pyplot

import icfa_tutorial.opt

if t.TYPE_CHECKING:
    from matplotlib.axes import Axes
    from matplotlib.figure import Figure


class RenderOnEval(loops.Callback):
    """Callback that renders on each objective function evaluation."""

    figure: Figure
    ax_actors: Axes
    ax_obj: Axes

    def __init__(self, title: str) -> None:
        super().__init__(title)
        self.indices = array("i")
        self.actors: t.List[array] = []
        self.objective = array("d")
        self.figure, [self.ax_actors, self.ax_obj] = pyplot.subplots(
            nrows=2, sharex=True
        )

    def run_begin(self, msg: loops.RunBeginMessage) -> None:
        self.figure.suptitle(msg.problem_id)

    def optimization_begin(self, msg: loops.OptBeginMessage) -> None:
        self.ax_actors.clear()
        for name in msg.param_names:
            self.ax_actors.plot([], [], label=name)
            self.actors.append(array("d"))
        self.ax_actors.legend()
        self.ax_actors.set_ylabel("Actor settings")
        self.ax_actors.grid()
        self.ax_obj.clear()
        self.ax_obj.plot([], [])
        self.ax_obj.set_xlabel("Iteration")
        self.ax_obj.set_ylabel(msg.objective_name)
        self.ax_obj.grid()

    def objective_evaluated(self, msg: loops.ObjectiveEvalMessage) -> None:
        # Copy data.
        self.indices.append(msg.index)
        for actor, value in zip(self.actors, msg.param_values):
            actor.append(value)
        self.objective.append(msg.objective)
        # Update lines.
        for line, actor in zip(self.ax_actors.lines, self.actors):
            line.set_data(self.indices, actor)
        [line] = self.ax_obj.lines
        line.set_data(self.indices, self.objective)
        # Update data limits.
        self.ax_actors.dataLim.update_from_data_x(msg.index, ignore=False)
        self.ax_actors.set_ylim(
            bottom=msg.optimization_space.low.min(),
            top=msg.optimization_space.high.max(),
        )
        self.ax_obj.dataLim.update_from_data_x(msg.index)
        self.ax_obj.set_ylim(msg.objective_range)
        # Update view.
        self.ax_actors.autoscale_view(scaley=False)
        self.ax_obj.autoscale_view(scaley=False)
        self.figure.canvas.draw_idle()
        pyplot.pause(0.1)

    def run_end(self, msg: loops.RunEndMessage) -> None:
        self.figure.tight_layout()
        self.figure.canvas.draw_idle()


def main() -> None:
    """Main function."""
    factory = loops.RunFactory()
    factory.optimizer_factory = Cobyla()
    factory.callback = RenderOnEval("RenderOnEval")
    factory.select_problem("FakeSteeringEnv-v0")
    run = factory.build()
    with closing(run.problem):
        run.run_full_optimization()
    pyplot.show()


if __name__ == "__main__":
    main()
