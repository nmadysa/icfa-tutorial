"""An environment using the fake accelerator."""

from __future__ import annotations

import typing as t

import numpy as np
from cernml import coi, japc_utils
from cernml.coi import cancellation
from gym import spaces

from .mockjapc import MockJapc
from .simulator import Fake

if t.TYPE_CHECKING:
    from pyjapc import PyJapc


class FakeSteeringEnv(coi.SingleOptimizable):
    metadata = {
        "render.modes": [],
        "cern.machine": coi.Machine.NO_MACHINE,
        "cern.cancellable": True,
        "cern.japc": False,
    }

    param_names = [
        "FAKE.BUMP1/K",
        "FAKE.BUMP2/K",
        "FAKE.QUAD1/K",
    ]
    objective_name = "FAKE.BCT/Acquisition#Loss"
    objective_range = (0.0, 5.0)

    RAW_SPACE = spaces.Dict(
        {
            "FAKE.BUMP1/K": spaces.Box(-1.5, 1.5, shape=(), dtype=np.float64),
            "FAKE.BUMP2/K": spaces.Box(-1.5, 1.5, shape=(), dtype=np.float64),
            "FAKE.QUAD1/K": spaces.Box(
                -np.pi / 2, np.pi / 2, shape=(), dtype=np.float64
            ),
        }
    )

    def __init__(
        self,
        japc: t.Optional[PyJapc] = None,
        cancellation_token: t.Optional[cancellation.Token] = None,
    ) -> None:
        self.token = cancellation_token or cancellation.Token()
        self.japc = japc or t.cast("PyJapc", MockJapc(Fake()))
        self.stream = japc_utils.subscribe_stream(
            self.japc, self.objective_name, token=self.token
        )
        self.stream.start_monitoring()
        self.optimization_space = spaces.flatten_space(self.RAW_SPACE)

    def close(self) -> None:
        self.stream.stop_monitoring()

    def get_initial_params(self) -> np.ndarray:
        space = self.optimization_space
        return np.zeros(space.shape, dtype=space.dtype)

    def compute_single_objective(self, params: np.ndarray) -> float:
        settings = spaces.unflatten(self.RAW_SPACE, params)
        for name, value in settings.items():
            self.japc.setParam(name, value)
        try:
            value, _ = self.stream.wait_for_next()
        except cancellation.CancelledError:
            self.token.complete_cancellation()
            raise
        return value


coi.register("FakeSteeringEnv-v0", entry_point=FakeSteeringEnv)
