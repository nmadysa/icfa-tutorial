"""An environment using the fake accelerator."""

from __future__ import annotations

import typing as t

import numpy as np
from cernml import coi, japc_utils, mpl_utils
from cernml.coi import cancellation
from gym import Env, spaces

from .mockjapc import MockJapc
from .simulator import Fake

if t.TYPE_CHECKING:
    from matplotlib.figure import Figure
    from pyjapc import PyJapc


class FakeSteeringChronoEnv(Env):
    metadata = {
        "render.modes": [],
        "cern.machine": coi.Machine.NO_MACHINE,
        "cern.cancellable": True,
        "cern.japc": False,
    }

    param_names = [
        "FAKE.BUMP1/K",
        "FAKE.BUMP2/K",
        "FAKE.QUAD1/K",
    ]
    objective_name = "FAKE.BCT/Acquisition#Loss"
    reward_range = (0.0, 5.0)

    RAW_SPACE = spaces.Dict(
        {
            "FAKE.BUMP1/K": spaces.Box(-1.5, 1.5, shape=(), dtype=np.float64),
            "FAKE.BUMP2/K": spaces.Box(-1.5, 1.5, shape=(), dtype=np.float64),
            "FAKE.QUAD1/K": spaces.Box(
                -np.pi / 2, np.pi / 2, shape=(), dtype=np.float64
            ),
        }
    )

    def __init__(
        self,
        japc: t.Optional[PyJapc] = None,
        cancellation_token: t.Optional[cancellation.Token] = None,
    ) -> None:
        self.token = cancellation_token or cancellation.Token()
        self.japc = japc or t.cast("PyJapc", MockJapc(Fake()))
        self.stream = japc_utils.subscribe_stream(
            self.japc, self.objective_name, token=self.token
        )
        self.stream.start_monitoring()
        self.action_space = spaces.flatten_space(self.RAW_SPACE)
        self.observation_space = spaces.Box(
            *self.reward_range, shape=(10,), dtype=np.float64
        )
        self.current_params = self.action_space.sample()
        self.history = self.observation_space.high.copy()
        self.renderer = mpl_utils.make_renderer(self.update_figure)

    def close(self) -> None:
        self.stream.stop_monitoring()

    def reset(self) -> np.ndarray:
        self.current_params = self.action_space.sample()
        self._transmit_current_settings()
        self.history = self.observation_space.high.copy()
        return self.history.copy()

    def step(
        self, action: np.ndarray
    ) -> t.Tuple[np.ndarray, float, bool, t.Dict[str, t.Any]]:
        self._update_settings(action)
        self._transmit_current_settings()
        self._update_history()
        obs = self.history.copy()
        reward = -self.history[0]
        done = False
        return obs, reward, done, {}

    def _update_settings(self, action: np.ndarray) -> None:
        new_params = self.current_params + 0.1 * action
        new_settings = spaces.unflatten(self.RAW_SPACE, new_params)
        if new_settings in self.RAW_SPACE:
            self.current_params = new_params

    def _transmit_current_settings(self) -> None:
        settings = spaces.unflatten(self.RAW_SPACE, self.current_params)
        for name, value in settings.items():
            self.japc.setParam(name, value)

    def _update_history(self) -> None:
        loss, _ = self.stream.wait_for_next()
        self.history = np.roll(self.history, 1)
        self.history[0] = loss

    def render(self, mode: str = "human") -> mpl_utils.MatplotlibFigures:
        if mode in ["human", "matplotlib_figures"]:
            return self.renderer.update(mode)
        return super().render(mode)

    def update_figure(self, figure: Figure) -> t.Iterator[None]:
        axes = figure.add_subplot(111)
        axes.set_xlabel("Most recent losses")
        axes.set_ylabel("Loss")
        axes.grid()
        axes.set_ylim(*self.reward_range)
        [line] = axes.plot(self.history, "o")
        while True:
            yield
            line.set_ydata(self.history)


coi.register(
    "FakeSteeringChronoEnv-v0", entry_point=FakeSteeringChronoEnv, max_episode_steps=20
)
