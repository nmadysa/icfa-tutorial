"""Test the JAPC utilities."""

# pylint: disable = missing-class-docstring
# pylint: disable = missing-function-docstring
# pylint: disable = redefined-outer-name

from __future__ import annotations

import datetime
import threading
import time
import typing as t
from logging import getLogger
from unittest.mock import Mock

LOG = getLogger(__name__)


class Simulator:
    def user(self) -> str:
        raise NotImplementedError()

    def has(self, name: str) -> bool:
        raise NotImplementedError()

    def get(self, name: str) -> t.Any:
        raise NotImplementedError()

    def set(self, name: str, value: t.Any) -> None:
        raise NotImplementedError()


class MockJapc:
    """A minimal mock of PyJapc.

    This only provides a :meth:`subscribeParam()` method that returns a
    mock handle. The values that are published by this mock handle are
    determined in the constructor.

    The argument is either a list or a mapping from strings to lists. In
    the former case, the same list (or rather, a copy) is passed to each
    subscription handle as a list of mock values. In the latter case,
    the subscription parameter's name is used as a key into the mapping
    to retrieve the list of mock values.
    """

    # pylint: disable = invalid-name
    # pylint: disable = too-few-public-methods

    TIME_STEP_SECONDS = 1.00

    def __init__(self, simulator: Simulator) -> None:
        self.simulator = simulator

    def setParam(self, name: str, value: t.Any) -> None:
        self.simulator.set(name, value)

    @t.overload
    def getParam(self, name_or_names: str) -> t.Any:
        ...

    @t.overload
    def getParam(self, name_or_names: t.List[str]) -> t.List[t.Any]:
        ...

    def getParam(self, name_or_names: t.Union[str, t.List[str]]) -> t.Any:
        if isinstance(name_or_names, str):
            return self.simulator.get(name_or_names)
        return [self.simulator.get(name) for name in name_or_names]

    def subscribeParam(
        self,
        name: t.Union[str, t.List[str]],
        onValueReceived: t.Callable,
        onException: t.Callable,
        **kwargs: t.Any,
    ) -> MockSubscriptionHandle:
        return MockSubscriptionHandle(
            name_or_names=name,
            on_value=onValueReceived,
            on_exception=onException,
            simulator=self.simulator,
            time_step=self.TIME_STEP_SECONDS,
            **kwargs,
        )


class MockSubscriptionHandle:
    """Return value of :class:`MockJapc.subscribeParam()`.

    This is a mock of PyJapc subscription handles. It contains a list of
    arbitrary values. Whenever monitoring starts, it spins up a thread
    that publishes each item of this list in turn. Once it has iterated
    over this list, the thread ends and no further items are published.
    When monitoring is stopped and restarted, iteration starts from the
    beginning. The time between two item publications is at least
    :attr:`time_step` seconds.
    """

    def __init__(
        self,
        name_or_names: t.Union[str, t.List[str]],
        *,
        on_value: t.Callable,
        on_exception: t.Callable,
        simulator: Simulator,
        time_step: float,
        **kwargs,
    ) -> None:
        if isinstance(name_or_names, str):
            if not simulator.has(name_or_names):
                raise ValueError(f"unknown device: {name_or_names!r}")
        else:
            for name in name_or_names:
                if not simulator.has(name):
                    raise ValueError(f"unknown device: {name!r}")
        self.thread: t.Optional[threading.Thread] = None
        self.name = name_or_names
        self.on_value = on_value
        self.on_exception = on_exception
        self.simulator = simulator
        self.time_step = time_step
        self.init_kwargs = kwargs

    # pylint: disable = invalid-name

    def isMonitoring(self) -> bool:
        return bool(self.thread)

    def startMonitoring(self) -> None:
        if self.thread:
            return
        self.thread = threading.Thread(target=self._thread_func)
        self.thread.start()

    def stopMonitoring(self) -> None:
        if not self.thread:
            return
        thread = self.thread
        self.thread = None
        thread.join()

    def getParameter(self) -> Mock:
        if isinstance(self.name, str):
            parameter = Mock()
            parameter.getName.return_value = self.name
            return parameter
        raise AttributeError("getParameter")

    def getParameterGroup(self) -> Mock:
        if isinstance(self.name, str):
            raise AttributeError("getParameter")
        parameter_group = Mock()
        parameter_group.getNames.return_value = list(self.name)
        return parameter_group

    # pylint: enable = invalid-name

    def _mock_headers(self) -> t.Union[t.Dict[str, t.Any], t.List[t.Dict[str, t.Any]]]:
        user = self.simulator.user()
        now = datetime.datetime.now()
        if isinstance(self.name, str):
            return mock_header(user, now)
        assert isinstance(self.name, list)
        return [mock_header(user, now) for _ in range(len(self.name))]

    def _thread_func(self) -> None:
        LOG.debug("starting thread")
        assert self.on_value is not None
        assert self.on_exception is not None
        while True:
            time.sleep(self.time_step)
            if not self.isMonitoring():
                break
            try:
                if isinstance(self.name, str):
                    value = self.simulator.get(self.name)
                else:
                    value = list(map(self.simulator.get, self.name))
            except StopIteration:
                break
            except Exception as exc:  # pylint: disable=broad-except
                self.on_exception(self.name, str(exc), exc)
            LOG.debug("sending %s", value)
            self.on_value(self.name, value, self._mock_headers())
        LOG.debug("terminating thread")


def mock_header(selector: str, stamp: datetime.datetime) -> t.Dict[str, t.Any]:
    keys = {
        "acqStamp": stamp,
        "cycleStamp": stamp,
        "setStamp": stamp,
        "selector": selector,
        "isFirstUpdate": True,
        "isImmediateUpdate": True,
    }
    return {key: Mock() for key in keys}
