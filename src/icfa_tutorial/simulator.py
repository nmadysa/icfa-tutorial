"""A test accelerator."""

from __future__ import annotations

import typing as t

import numpy as np
from gym import spaces

from .mockjapc import Simulator

if t.TYPE_CHECKING:
    # pylint: disable = unused-import, ungrouped-imports
    from numpy.typing import ArrayLike


class Fake(Simulator):
    """The Fast Accelerator for Kaon Experiments. :)"""

    STEERERS = spaces.Dict(
        {
            "FAKE.BUMP1/K": spaces.Box(-1.5, 1.5, shape=(), dtype=np.float64),
            "FAKE.BUMP2/K": spaces.Box(-1.5, 1.5, shape=(), dtype=np.float64),
            "FAKE.QUAD1/K": spaces.Box(
                -np.pi / 2, np.pi / 2, shape=(), dtype=np.float64
            ),
        }
    )

    BCT_NAME = "FAKE.BCT/Acquisition#Loss"
    BCT_RANGE = (0.0, 5.0)

    def __init__(self, rng: t.Optional[np.random.Generator] = None) -> None:
        self._settings = self.STEERERS.sample()
        self._rng = rng or np.random.default_rng()

    def user(self) -> str:
        return "FAKE.USER.ALL"

    def has(self, name: str) -> bool:
        return name == self.BCT_NAME or name in self._settings

    def set(self, name: str, value: float) -> None:
        try:
            space = self.STEERERS[name]
        except KeyError:
            if name == self.BCT_NAME:
                raise ValueError(f"cannot set read-only device: {name!r}") from None
            raise ValueError(f"unknown device: {name!r}") from None
        array = np.asfarray(value, dtype=space.dtype)
        if array.shape != space.shape:
            raise ValueError(f"value not a scalar for {name}: shape is {array.shape}")
        if array not in space:
            raise ValueError(f"value out of bounds for {name}: {value}")
        self._settings[name] = array

    def get(self, name) -> float:
        array = self._settings.get(name)
        if array is not None:
            return float(array.item())
        if name == self.BCT_NAME:
            return bct_func(
                bump1=self._settings["FAKE.BUMP1/K"],
                bump2=self._settings["FAKE.BUMP2/K"],
                quad=self._settings["FAKE.QUAD1/K"],
                rng=self._rng,
            )
        raise ValueError(f"unknown device: {name!r}") from None


@t.overload
def bct_func(
    bump1: float,
    bump2: float,
    quad: float,
    rng: np.random.Generator,
) -> float:
    ...


@t.overload
def bct_func(
    bump1: np.ndarray,
    bump2: np.ndarray,
    quad: float,
    rng: np.random.Generator,
) -> np.ndarray:
    ...


def bct_func(
    bump1: ArrayLike,
    bump2: ArrayLike,
    quad: float,
    rng: np.random.Generator,
) -> ArrayLike:
    """The loss function of our simulation."""
    opt_quad = 1.2
    co_axis = np.cos(quad) * bump1 + np.sin(quad) * bump2
    contra_axis = -np.sin(quad) * bump1 + np.cos(quad) * bump2
    loss = np.log(
        1.0
        + 8 * (quad - opt_quad) ** 2
        + 16 * (co_axis) ** 2
        + ((contra_axis) ** 4 - 4 * (contra_axis) ** 2 + 4.0)
    )
    loss *= np.exp(rng.normal(scale=0.01, size=loss.shape))
    return loss


def boxspace(box: spaces.Box, num: int = 50, axis: int = 0) -> np.ndarray:
    """Like `linspace()`, but take limits from a ~`gym.spaces.Box`."""
    return np.linspace(
        t.cast(np.ndarray, box.low),
        t.cast(np.ndarray, box.high),
        num,
        dtype=box.dtype,
        axis=axis,
        endpoint=True,
        retstep=False,
    )


def plot_bct() -> None:
    """Main function that shows the phase-space we're dealing with."""
    # pylint: disable = import-outside-toplevel
    import matplotlib as mpl
    from matplotlib import pyplot

    xlist = boxspace(Fake.STEERERS["FAKE.BUMP1/K"], 21)
    ylist = boxspace(Fake.STEERERS["FAKE.BUMP2/K"], 21)
    zlist = boxspace(Fake.STEERERS["FAKE.QUAD1/K"], 9)
    rng = np.random.default_rng()
    norm = mpl.colors.Normalize(*Fake.BCT_RANGE)
    pyplot.subplots(3, 3, sharex=True, sharey=True)
    for i, zval in enumerate(zlist):
        losses = bct_func(xlist.reshape(1, -1), ylist.reshape(-1, 1), zval, rng=rng)
        pyplot.subplot(3, 3, i + 1)
        pyplot.contourf(xlist, ylist, losses, norm=norm)
        pyplot.colorbar()
        pyplot.title(f"FAKE.QUAD1/K = {zval:.2g}")
        if i >= 6:
            pyplot.xlabel("FAKE.BUMP1/K")
        if not i % 3:
            pyplot.ylabel("FAKE.BUMP2/K")
    pyplot.tight_layout()
    pyplot.show()


if __name__ == "__main__":
    plot_bct()
